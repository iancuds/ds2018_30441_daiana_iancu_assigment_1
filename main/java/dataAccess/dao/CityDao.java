package dataAccess.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import dataAccess.model.City;

public class CityDao {

	


			
				private static final Log LOGGER = LogFactory.getLog(CityDao.class);

				private SessionFactory factory;

				public CityDao(SessionFactory factory) {
					this.factory = factory;
				}

				public City addCity(City city) {
					int idCity = -1;
					Session session = factory.openSession();
					Transaction tx = null;
					try {
						tx = session.beginTransaction();
						idCity = (int) session.save(city);
						city.setIdCity(idCity);
						tx.commit();
					} catch (HibernateException e) {
						if (tx != null) {
							tx.rollback();
						}
						LOGGER.error("", e);
					} finally {
						session.close();
					}
					return city;
				}

				@SuppressWarnings("unchecked")
				public List<City> findCitys() {
					Session session = factory.openSession();
					Transaction tx = null;
					List<City> citys = null;
					try {
						tx = session.beginTransaction();
						citys = session.createQuery("FROM City").list();
						tx.commit();
					} catch (HibernateException e) {
						if (tx != null) {
							tx.rollback();
						}
						LOGGER.error("", e);
					} finally {
						session.close();
					}
					return citys;
				}

				@SuppressWarnings("unchecked")
				public City findCity(int id) {
					Session session = factory.openSession();
					Transaction tx = null;
					List<City> citys = null;
					try {
						tx = session.beginTransaction();
						Query query = session.createQuery("FROM City WHERE idcity = :id");
						query.setParameter("id", id);
						citys = query.list();
						tx.commit();
					} catch (HibernateException e) {
						if (tx != null) {
							tx.rollback();
						}
						LOGGER.error("", e);
					} finally {
						session.close();
					}
					return citys != null && !citys.isEmpty() ? citys.get(0) : null;
				}
				
				public City deleteCity(int id)
				{
					City city = findCity(id);
					Session session =factory.openSession();
					Transaction tx = null;
					
					
					try {
						tx = session.beginTransaction();
					//	Query query = session.createQuery("DELETE FROM `table_name` WHERE idcity = :id");
					   session.delete(city);
						tx.commit();
						return city;
					} catch (HibernateException e) {
						if (tx != null) {
							tx.rollback();
						}
						LOGGER.error("", e);
					} finally {
						session.close();
					}
					return null;
				}
			

		

		
	

}
