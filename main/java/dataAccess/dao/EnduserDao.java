package dataAccess.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import dataAccess.model.Enduser;

public class EnduserDao {



		
			private static final Log LOGGER = LogFactory.getLog(EnduserDao.class);

			private SessionFactory factory;

			public EnduserDao(SessionFactory factory) {
				this.factory = factory;
			}

			public Enduser addEnduser(Enduser enduser) {
				int idEnduser = -1;
				Session session = factory.openSession();
				Transaction tx = null;
				try {
					tx = session.beginTransaction();
					idEnduser = (int) session.save(enduser);
					enduser.setIdEnduser(idEnduser);
					tx.commit();
				} catch (HibernateException e) {
					if (tx != null) {
						tx.rollback();
					}
					LOGGER.error("", e);
				} finally {
					session.close();
				}
				return enduser;
			}

			@SuppressWarnings("unchecked")
			public List<Enduser> findEndusers() {
				Session session = factory.openSession();
				Transaction tx = null;
				List<Enduser> endusers = null;
				try {
					tx = session.beginTransaction();
					endusers = session.createQuery("FROM Enduser").list();
					tx.commit();
				} catch (HibernateException e) {
					if (tx != null) {
						tx.rollback();
					}
					LOGGER.error("", e);
				} finally {
					session.close();
				}
				return endusers;
			}

			
			
			
			@SuppressWarnings("unchecked")
			public Enduser findEnduser(String email, String passwd) {
				Session session = factory.openSession();
				Transaction tx = null;
				List<Enduser> endusers = null;
				try {
					tx = session.beginTransaction();
					Query query = session.createQuery("FROM Enduser WHERE email = :email AND passwd= :passwd");
					query.setParameter("email", email);
					query.setParameter("passwd", passwd);
					endusers = query.list();
					tx.commit();
				} catch (HibernateException e) {
					if (tx != null) {
						tx.rollback();
					}
					LOGGER.error("", e);
				} finally {
					session.close();
				}
				return endusers != null && !endusers.isEmpty() ? endusers.get(0) : null;
			}
			
		
	

	
}
