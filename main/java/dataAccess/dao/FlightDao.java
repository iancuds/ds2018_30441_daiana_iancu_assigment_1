package dataAccess.dao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import dataAccess.model.Flight;

import java.util.List;

public class FlightDao {




	

	
		private static final Log LOGGER = LogFactory.getLog(FlightDao.class);

		private SessionFactory factory;

		public FlightDao(SessionFactory factory) {
			this.factory = factory;
		}

		public Flight addFlight(Flight flight) {
			int idFlight = -1;
			Session session = factory.openSession();
			Transaction tx = null;
			try {
				tx = session.beginTransaction();
				idFlight = (int) session.save(flight);
				flight.setIdFlight(idFlight);
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return flight;
		}

		@SuppressWarnings("unchecked")
		public List<Flight> findFlights() {
			Session session = factory.openSession();
			Transaction tx = null;
			List<Flight> flights = null;
			try {
				tx = session.beginTransaction();
				flights = session.createQuery("FROM Flight").list();
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return flights;
		}

		@SuppressWarnings("unchecked")
		public Flight findFlight(int id) {
			Session session = factory.openSession();
			Transaction tx = null;
			List<Flight> flights = null;
			try {
				tx = session.beginTransaction();
				Query query = session.createQuery("FROM Flight WHERE idflight = :id");
				query.setParameter("id", id);
				flights = query.list();
				tx.commit();
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return flights != null && !flights.isEmpty() ? flights.get(0) : null;
		}
		
		public Flight deleteFlight(int id)
		{
			Flight flight = findFlight(id);
			Session session =factory.openSession();
			Transaction tx = null;
			
			
			try {
				tx = session.beginTransaction();
			//	Query query = session.createQuery("DELETE FROM `table_name` WHERE idflight = :id");
			   session.delete(flight);
				tx.commit();
				return flight;
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return null;
		}
		
		public Flight updateFlight(Flight flight )
		{
			
			Session session =factory.openSession();
			Transaction tx = null;
			
			
			try {
				tx = session.beginTransaction();

			   session.update(flight);
				tx.commit();
				return flight;
			} catch (HibernateException e) {
				if (tx != null) {
					tx.rollback();
				}
				LOGGER.error("", e);
			} finally {
				session.close();
			}
			return null;
		}
	

}
