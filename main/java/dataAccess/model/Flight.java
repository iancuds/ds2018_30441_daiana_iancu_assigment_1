package dataAccess.model;



public class Flight {
	private int idFlight;
	private City arrivalCityId;
	private City departureCityId;
	private String arrivalTime;
	private String departureTime;
	private int flightNumber;
	private String airplaneType;
	
	
	public Flight()
	{}

	public int getIdFlight() {
		return idFlight;
	}

	public void setIdFlight(int idFlight) {
		this.idFlight = idFlight;
	}


	public City getArrivalCityId() {
		return arrivalCityId;
	}

	public void setArrivalCityId(City arrivalCityId) {
		this.arrivalCityId = arrivalCityId;
	}

	public City getDepartureCityId() {
		return departureCityId;
	}

	public void setDepartureCityId(City departureCityId) {
		this.departureCityId = departureCityId;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	@Override
	public String toString() {
		return "Flight [idFlight=" + idFlight + ", arrivalCityId=" + arrivalCityId + ", departureCityId="
				+ departureCityId + ", arrivalTime=" + arrivalTime + ", departureTime=" + departureTime
				+ ", flightNumber=" + flightNumber + ", airplaneType=" + airplaneType + "]";
	}

	
	
	
	
}
