package dataAccess.model;

import java.util.Set;

public class City {

	
	private int idCity;
	private float latitude;
	private float longitude;
	private Set<Flight> arrivalFlights;
	private Set<Flight> departureFlights;
	
	
	public City() {}

	public int getIdCity() {
		return idCity;
	}

	public void setIdCity(int idCity) {
		this.idCity = idCity;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}


	public Set<Flight> getArrivalFlights() {
		return arrivalFlights;
	}

	public void setArrivalFlights(Set<Flight> arrivalFlights) {
		this.arrivalFlights = arrivalFlights;
	}

	public Set<Flight> getDepartureFlights() {
		return departureFlights;
	}

	public void setDepartureFlights(Set<Flight> departureFlights) {
		this.departureFlights = departureFlights;
	}

	@Override
	public String toString() {
		return "City [idCity=" + idCity + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}

	
	
	
	
	
}
