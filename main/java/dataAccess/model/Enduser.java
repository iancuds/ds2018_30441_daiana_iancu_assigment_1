package dataAccess.model;

public class Enduser {

	private int idEnduser;
	private String email;
	private String passwd;
	private int isAdmin;
	
	public Enduser(int idEnduser, String email, String passwd, int isAdmin) {
		super();
		this.idEnduser = idEnduser;
		this.email = email;
		this.passwd = passwd;
		this.isAdmin = isAdmin;
	}
	
	public Enduser() {}

	public int getIdEnduser() {
		return idEnduser;
	}

	public void setIdEnduser(int idEnduser) {
		this.idEnduser = idEnduser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public int getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}

	@Override
	public String toString() {
		return "Enduser [idEnduser=" + idEnduser + ", email=" + email + ", passwd=" + passwd + ", isAdmin=" + isAdmin
				+ "]";
	}
	
	
	
	
}
