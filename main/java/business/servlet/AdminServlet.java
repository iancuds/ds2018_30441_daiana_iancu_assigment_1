package business.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import dataAccess.dao.FlightDao;
import dataAccess.model.City;
import dataAccess.model.Flight;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private FlightDao fd;
	String message = "";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        fd = new FlightDao(new Configuration().configure().buildSessionFactory());
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		PrintWriter out =response.getWriter();
		Cookie[] cookies =request.getCookies();
		for(Cookie c:cookies)
		{
			if(c.getName().equals("role"))
			{
				if(!(c.getValue().equals("admin")))
				{
					out.println("Access Denied!");
				}
				else
				{
					out.println("<!DOCTYPE html>\r\n" + 
							"<html>\r\n" + 
							"<head>\r\n" + 
							"<meta charset=\"ISO-8859-1\">\r\n" + 
							"<title>Admin Page</title>\r\n" + 
							"</head>\r\n" + 
							"<body>\r\n" + 
							"	<h1>Admin page</h1><form action=\"AdminServlet\" method=\"post\">\r\n" + 
							"	Flight Number: <input type=\"text\" name=\"nr\"/>\r\n" + "<br/>"+
							"	Type: <input type=\"text\" name=\"type\"/>\r\n" + "<br/>"+
							"	Arrival City: <input type=\"text\" name=\"arrivalC\"/>\r\n" + "<br/>"+
							"	Arrival Time: <input type=\"text\" name=\"arrivalT\"/>\r\n" + "<br/>"+
							"	Departure City: <input type=\"text\" name=\"depC\"/>\r\n" + "<br/>"+
							"	Departure Time: <input type=\"text\" name=\"depT\"/>\r\n" + "<br/>"+
							"	<input type=\"submit\" value=\"Save\"/>\r\n" + "<br/>"+
							"	<input type=\"hidden\" value=\"Save\" name = \"action\"/>\r\n" + "<br/>"+
							"	\r\n" + 
							" 	</form>\r\n" + 
							" 	\r\n" + 
							" 	<form action=\"AdminServlet\" method=\"post\">\r\n" + 
							"   Id: <input type=\"text\" name=\"id\"/>\r\n" + "<br/>" +
							"	Flight Number: <input type=\"text\" name=\"nr\"/>\r\n" + "<br/>"+
							"	Type: <input type=\"text\" name=\"type\"/>\r\n" + "<br/>"+
							"	Arrival City: <input type=\"text\" name=\"arrivalC\"/>\r\n" + "<br/>"+
							"	Arrival Time: <input type=\"text\" name=\"arrivalT\"/>\r\n" + "<br/>"+
							"	Departure City: <input type=\"text\" name=\"depC\"/>\r\n" + "<br/>"+
							"	Departure Time: <input type=\"text\" name=\"depT\"/>\r\n" + "<br/>"+
							"	<input type=\"submit\" value=\"Update\"/>\r\n" + "<br/>"+
							"	<input type=\"hidden\" value=\"Update\" name = \"action\"/>\r\n" + "<br/>"+
							"	</form>\r\n" + 
							"	\r\n" + 
							"	<form action=\"AdminServlet\" method=\"post\">\r\n" + "<br/>"+
							"	Flight ID: <input type=\"text\" name=\"id\"/>\r\n" + "<br/>"+
							"	<input type=\"submit\" value=\"Delete\"/>\r\n" + "<br/>"+
							"	<input type=\"hidden\" value=\"Delete\" name = \"action\"/>\r\n" + "<br/>"+
							"	\r\n" + 
							" 	</form>\r\n" + 
							" 	\r\n" + 
							" 	<form action=\"AdminServlet\" method=\"post\">\r\n" + "<br/>"+
							"	Flight ID: <input type=\"text\" name=\"id\"/>\r\n" + "<br/>"+
							"	<input type=\"submit\" value=\"Find\"/>\r\n" + "<br/>"+
							"	<input type=\"hidden\" value=\"Find\" name = \"action\"/>\r\n" + "<br/>"+
							"	\r\n" + 
							" 	</form>\r\n" + 
							" 	\r\n" + 
							" <h2> Result</h2>"+
							"<p>" +message+"</p>"+
							"</body>\r\n" + 
							"</html>");
					
				}
			}
		}
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String act =  request.getParameter("action");
		switch(act)
		{
		case "Delete":
		{
			int id = Integer.parseInt(request.getParameter("id"));
			fd.deleteFlight(id);
			message="Successfully deleted!";
			break;
		}
		case "Find":
		{
			int id = Integer.parseInt(request.getParameter("id"));
			Flight flight = fd.findFlight(id);
			message = flight.toString();
			break;
		}
		case "Save":
		{
			System.out.println("in save");
			int nr = Integer.parseInt(request.getParameter("nr"));
			String planeType=request.getParameter("type");
			int arrivalC = Integer.parseInt(request.getParameter("arrivalC"));
			String arrivalT=request.getParameter("arrivalT");
			int depC = Integer.parseInt(request.getParameter("depC"));
			String depT=request.getParameter("depT");
			
			Flight newFlight = new Flight();
	
			newFlight.setAirplaneType(planeType);
			City c1 = new City();
			c1.setIdCity(arrivalC);
			newFlight.setArrivalCityId(c1);
			newFlight.setArrivalTime(arrivalT);
			City c2 = new City();
			c2.setIdCity(depC);
			newFlight.setDepartureCityId(c2);
			newFlight.setDepartureTime(depT);
			newFlight.setFlightNumber(nr);
			
			
			Flight f =fd.addFlight(newFlight);
			System.out.println(f);
			message = f.toString();
			break;
		}
		case "Update":
		{
			int id = Integer.parseInt(request.getParameter("id"));
			int nr = Integer.parseInt(request.getParameter("nr"));
			String planeType=request.getParameter("type");
			int arrivalC = Integer.parseInt(request.getParameter("arrivalC"));
			String arrivalT=request.getParameter("arrivalT");
			int depC = Integer.parseInt(request.getParameter("depC"));
			String depT=request.getParameter("depT");
			
			Flight flight=new Flight();
			flight.setIdFlight(id);
			flight.setAirplaneType(planeType);
			City c1 = new City();
			c1.setIdCity(arrivalC);
			flight.setArrivalCityId(c1);
			flight.setArrivalTime(arrivalT);
			City c2 = new City();
			c2.setIdCity(depC);
			flight.setDepartureCityId(c2);
			flight.setDepartureTime(depT);
			flight.setFlightNumber(nr);
			
			fd.updateFlight(flight);
			message = "Successfully updated!";
			break;
			
	
		
		}
		}
		
		doGet(request, response);
	}

}
