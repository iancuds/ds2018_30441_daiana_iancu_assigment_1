package business.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import dataAccess.dao.FlightDao;
import dataAccess.model.Flight;

/**
 * Servlet implementation class ClientServlet
 */
@WebServlet("/ClientServlet")
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDao fd;
	String message="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientServlet() {
        super();
        fd = new FlightDao(new Configuration().configure().buildSessionFactory());
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		

		PrintWriter out =response.getWriter();
		Cookie[] cookies =request.getCookies();
		String email="";
		String role="";
		int found=0;
		for(Cookie c:cookies)
		{
			if(c.getName().equals("role"))
			{
				role=c.getValue();
				found=1;
			}
			else
			{
				email=c.getValue();
			}
			
			if(found==1)
			{
				
				if(!(role.equals("client")))
				{
					out.println("Access Denied!");
				}
				else
				{
					 out.println("<!DOCTYPE html>\r\n" + 
					 		"<html>\r\n" + 
					 		"<head>\r\n" + 
					 		"<meta charset=\"ISO-8859-1\">\r\n" + 
					 		"<title>Client Page</title>\r\n" + 
					 		"</head>\r\n" + 
					 		"<body>\r\n" + 
					 		"	<h1>Hello, "+ email +"</h1>\r\n" + 
					 		"	\r\n" + 
					 		"	<h2>Flights</h2>\r\n" + 
					 		"	<ul>");
					 List<Flight> flights = fd.findFlights();
					 
					 for(Flight f:flights)
					 {
						 out.println("<li> Flight:"+f.getFlightNumber()+" Departs from: latitude:"+f.getDepartureCityId().getLatitude()+" longitude:"+f.getDepartureCityId().getLongitude()+"at: "+f.getDepartureTime()+". To: latitude:"+f.getArrivalCityId().getLatitude()+" longitude:"+f.getArrivalCityId().getLongitude()+" at:"+f.getArrivalTime()+"</li>");
					 }
					 out.println("</ul>");
					 
					 out.println("<h2>Find the local time:</h2>\r\n" + 
					 		"	<form action=\"ClientServlet\" method=\"post\">\r\n" + 
					 		"	Latitude:<input type=\"text\" name=\"lat\"><br/>\r\n" + 
					 		"	Longitude:<input type=\"text\" name=\"long\"><br/>\r\n" + 
					 		"	<input type=\"submit\" value=\"Get Local Time\"/>\r\n" + 
					 		"	</form>\r\n" + 
					 		" 	<h2>Local Time: </h2>\r\n" + 
					 		"</body>\r\n" + 
					 		"</html>" +message);
					 
				}
			}
		}
				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String localTime="localtime";
		float latitude = Float.parseFloat(request.getParameter("lat"));
		float longitude = Float.parseFloat(request.getParameter("long"));
		
		message = localTime;
		
		
		doGet(request, response);
	}

}
