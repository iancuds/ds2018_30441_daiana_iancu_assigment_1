package business.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import dataAccess.dao.EnduserDao;
import dataAccess.dao.FlightDao;
import dataAccess.model.Enduser;

/**
 * Servlet implementation class LogIn
 */
@WebServlet("/LogIn")
public class LogIn extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	

	private EnduserDao ed;
	private FlightDao fd;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogIn() {
        super();
        ed = new EnduserDao(new Configuration().configure().buildSessionFactory());
		fd = new FlightDao(new Configuration().configure().buildSessionFactory());
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 
		PrintWriter out = response.getWriter();
	
		out.println("<!DOCTYPE html>	<html> <head>	<meta charset="+"ISO-8859-1"+">	<title>Log In</title>"+
		"</head>"+
		"<body>"+
			"<form action=\"LogIn\" method=\"post\">	Name: <input type=\"text\" name=\"email\"/>	<br/>Password: <input type=\"password\" name=\"password\">	<input type=\"submit\" value=\"Login\"/>	</form> </body></html>");
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		
		PrintWriter out = response.getWriter();
	
		String username=request.getParameter("email");
		String password =request.getParameter("password");
		
		
		Enduser user = ed.findEnduser(username, password);
		System.out.println(user);
		if(user != null)
		{
			if(user.getIsAdmin() == 1)
			{
				Cookie roleCk = new Cookie("role", "admin");
				roleCk.setMaxAge(30*60);
				Cookie ck=new Cookie("email", username);
				ck.setMaxAge(30*60);
				response.addCookie(ck);
				response.addCookie(roleCk);
				
				response.sendRedirect("AdminServlet");
			}
			else 
			{
				Cookie roleCk = new Cookie("role", "client");
				roleCk.setMaxAge(30*60);
				Cookie ck=new Cookie("email", username);
				ck.setMaxAge(30*60);
				response.addCookie(ck);
				response.addCookie(roleCk);
				
				response.sendRedirect("ClientServlet");
			}
		}
		else
		{
	
	     
			doGet(request, response);
		}
		
		

	}

}
