import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import dataAccess.dao.EnduserDao;
import dataAccess.dao.FlightDao;
import dataAccess.model.Enduser;

public class Test {

	
	private EnduserDao ed;
	private FlightDao fd;
	
	
	public Test() {
		
		ed = new EnduserDao(new Configuration().configure().buildSessionFactory());
		fd = new FlightDao(new Configuration().configure().buildSessionFactory());
	}
	 
	 
	 public static void main(String[] args)
	 {
		 Test t1 = new Test();
		 List<Enduser> el = t1.ed.findEndusers();
		 System.out.println(t1.fd.findFlight(1).toString());
		 
		 System.out.println(t1.ed.findEnduser("admin","admin"));

	 for(Enduser e:el)
	 {
		 System.out.println(e.getEmail()+" "+e.getIsAdmin());
	 }
	 }
}
